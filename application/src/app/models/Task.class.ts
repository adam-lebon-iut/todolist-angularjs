import * as moment from 'moment';
import { TodoComponent } from './TodoComponent';

export class Task extends TodoComponent {

    constructor(task?: Task) {
        super();

        if (task) {
            this.id = task.id || -1;
            this.name = task.name || '';
            this.priority = task.priority || 0;
            this.done = task.done || false;
            if (task.date instanceof moment) {
                this.date = task.date;
            } else if (typeof task.date === 'string') {
                this.date = moment(task.date);
            }
            this.notes = task.notes || '';
        }
    }
}
