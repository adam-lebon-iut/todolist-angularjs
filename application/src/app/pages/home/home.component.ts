import { Component } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { filter } from 'rxjs/operators';
import { TasksService } from 'src/app/services/tasks.service';
import { Task } from 'src/app/models/Task.class';
import { EditTaskDialogComponent } from '../editTaskDialog/editTaskDialog.component';
import * as moment from 'moment';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent {
    public variable = { a: 5, b: 9 };
    public tasks: Map<moment.Moment, Task[]>;

    public readonly PRIORITY = Task.PRIORITY;

    constructor(
        private dialogService: MatDialog,
        private taskService: TasksService,
        private router: Router,
        route: ActivatedRoute
    ) {
        this.updateTasks();

        router.events
            .pipe(filter((event) => event instanceof NavigationEnd))
            .subscribe((event) => {
                // If a subroute is active
                if (route.snapshot.firstChild !== null) {
                    const id = parseInt(route.snapshot.firstChild.paramMap.get('id'), 10);
                    if (isNaN(id)) {
                        this.openEditTaskDialog();
                    } else {
                        this.openEditTaskDialog(id);
                    }
                }
            });
    }

    public save(): void {
        this.taskService.save();
    }

    /**
     * Open the dialog to edit or create a task
     */
    private openEditTaskDialog(id: number = -1): void {
        this.dialogService.open(EditTaskDialogComponent, {
                width: 'calc(100vw - 20px)',
                maxWidth: '700px',

                height: 'calc(100vh - 20px)',
                data: id,
                autoFocus: id === -1,
                restoreFocus: false
            })
            .afterClosed()
            .subscribe(() => {
                this.updateTasks();
                this.router.navigate(['/']);
            });
    }

    /**
     * Met à jour la liste des tâches
     */
    private updateTasks(): void {
        this.tasks = this.taskService.groupByDate(this.taskService.getTasks());
    }
}
