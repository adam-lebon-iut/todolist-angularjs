import { Component } from '@angular/core';

@Component({
    selector: 'app-delete-dialog',
    template: `
        <h1 mat-dialog-title>Voulez-vous vraiment supprimer cette tâche</h1>

        <div mat-dialog-actions>
        <button mat-button [mat-dialog-close]="true">Oui</button>
        <button mat-button [mat-dialog-close]="false" cdkFocusInitial>Non</button>
        </div>
    `
})
export class DeleteDialogComponent {}
