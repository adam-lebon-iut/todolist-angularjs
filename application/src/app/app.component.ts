import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CordovaService } from './services/cordova.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(public title: Title, public cordova: CordovaService) {}

    public ngOnInit() {
        if (this.cordova.isCordova) {
            navigator.splashscreen.hide();
        }
    }
}
