import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatListModule, MatCheckboxModule, MatButtonModule, MatIconModule, MatDialogModule, MatInputModule, MatDatepickerModule, MatSelectModule, MatSnackBarModule, MatChipsModule, MAT_DATE_LOCALE } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { EditTaskDialogComponent } from './pages/editTaskDialog/editTaskDialog.component';
import { TasksService } from './services/tasks.service';
import { FormsModule } from '@angular/forms';
import { DeleteDialogComponent } from './pages/editTaskDialog/deleteDialog.component';
import { CordovaService } from './services/cordova.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EditTaskDialogComponent,
    DeleteDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatListModule,
    MatCheckboxModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatSelectModule,
    MatSnackBarModule,
    MatChipsModule
  ],
  providers: [
    TasksService,
    CordovaService,
    {provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [AppComponent],
  entryComponents: [DeleteDialogComponent]
})
export class AppModule { }
