import { Injectable } from '@angular/core';
import { Task } from '../models/Task.class';
import * as moment from 'moment';
import { GroupedTask } from '../models/GroupedTasks.class';
import { TodoComposite } from '../models/TodoComposite';

@Injectable()
export class TasksService {
    private tasks: TodoComposite = new TodoComposite();

    constructor() {
        const storedTasks = localStorage.getItem('tasks');
        if (storedTasks !== null) {
            this.tasks.fromJSON(storedTasks);
        }
    }

    public getTasks(): Task[] {
        // return this.tasks;
        return [];
    }

    // public groupByDate(tasksArray: Task[]): Map<moment.Moment, Task[]> {
    //     const tasksMap: Map<moment.Moment, Task[]> = new Map();
    //     for (const task of tasksArray) {
    //         const dueDate = task.dueDate;
    //         if (!tasksMap.has(dueDate)) {
    //             tasksMap.set(dueDate, []);
    //         }
    //         tasksMap.get(dueDate).push(task);
    //     }
    //     return tasksMap;
    // }

    public getById(id: number): Task {
        for (const task of this.tasks) {
            if (task.id === id) {
                return task;
            }
        }
        return null;
    }

    public addTask(task: Task): void {
        this.tasks.add(task);
        // On prend le plus grand id que l'on incrémente
        // task.id = this.tasks.reduce((a, b) => a > b ? a : b, { id: 0 }).id + 1;
        // this.tasks.push(task);
        this.save();
    }

    public deleteTask(task: Task): void {
        this.tasks.remove(task);
        // this.tasks.splice(this.tasks.indexOf(task), 1);
        this.save();
    }

    public save(): void {
        console.log('Saving task into localeStorage ...', this.tasks);
        // console.log(JSON.stringify(this.tasks));
        window.localStorage.setItem('tasks', this.tasks.toString());
    }
}
