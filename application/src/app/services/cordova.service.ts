import { Injectable } from '@angular/core';

@Injectable()
export class CordovaService {
    /**
     * Définie si l'on est dans un environment cordova ou non
     */
    public isCordova: boolean;

    constructor() {
        try {
            // tslint:disable-next-line:no-unused-expression
            device; // Déclenche une erreur si device n'existe pas
            this.isCordova = true;
        } catch (error) {
            this.isCordova = false;
        }
    }
}
